<?php

/**
 * Plugin Name: Ramuca CPT
 * Description: handle custom post types in ramuca.net
 * Version: 0.1
 * Requires at least: 5.2
 * Requires PHP: 7.0
 * Author: tejido.io
 * Author URI: https://tejido.io
 * License: GPLv2 or later
 * Text Domain: ramuca-cpt
 */

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright, 2020 Tejido S. Coop. And
*/

/**
* Check dependencies
*/

add_action('after_plugin_row_' . plugin_basename( __FILE__ ), 'ramuca_cpt_plugin_row', 5, 3);

function ramuca_cpt_plugin_row($plugin_file, $plugin_data, $status)
{
   if(is_plugin_active('advanced-custom-fields/acf.php')) return;  ?>
   <tr class="plugin-update-tr active acfe-plugin-tr">
       <td colspan="3" class="plugin-update colspanchange">
           <div class="update-message notice inline notice-error notice-alt">
               <p><?php _e('Este plugin depende de Advanced Custom Fields', 'ramuca-cpt'); ?>
               <br/>
                   <?php _e('Consíguelo aquí: ', 'ramuca-cpt') ?>
                   <a href="//wordpress.org/plugins/advanced-custom-fields/" target="_blank">
                     //wordpress.org/plugins/advanced-custom-fields/
                   </a>
               </p>
           </div>
      </td>
  </tr>
<?php
}

/**
* Change ACF Local JSON save/load location to /acf folder inside this plugin
*/
//
add_filter('acf/settings/save_json', function(){
   return dirname(__FILE__) . '/fields';
});
add_filter('acf/settings/load_json', function($paths) {
   $paths[] = dirname(__FILE__) . '/fields';
   return $paths;
});

/**
*  Registers a custom post type given an identifier and its singular and plural labels
*/

function ramuca_cpt_create($id, $singular, $plural, $overwrite=NULL)
{
   $labels = [
       'name' => _x($plural, 'post type general name', 'pst-cases'),
       'singular_name' => _x($singular, 'post type singular name', 'pst-cases'),
       'add_new' => _x('Añade ', $singular),
       'add_new_item' => __('Añade ' . $singular),
       'edit_item' => __('Edita ' . $singular),
       'new_item' => __('Nuevo ' . $singular),
       'all_items' => __('Todos'),
       'view_item' => __('Ver ' . $singular),
       'search_items' => __('Search ' . $plural),
       'not_found' => __('No se han encontrado ' . $plural),
       'not_found_in_trash' => __('No se ha encontrado nada en la papelera'),
       'parent_item_colon' => '',
       'menu_name' => $plural
   ];
   $settings = [
       'labels' => $labels,
       'description' => __('Muestra ' . $plural),
       'public' => true,
       'has_archive' => true,
       'rewrite' => [
           'slug' => sanitize_title($plural)
       ],
   ];
   if( isset($overwrite) ){
       foreach($overwrite as $k=>$v)
           $settings[$k] = $v;
   }

   register_post_type($id, $settings);
}

/**
*  Define and instantiate Novact custom post types
*/
add_action('init', 'ramuca_cpt_custom_post_types');

function ramuca_cpt_custom_post_types()
{
    $ramuca_cpt_custom_post_types = [
        [
           'od',
           __('Oferta/Demanda', 'ramuca-cpt'),
           __('Ofertas/Demandas', 'ramuca-cpt'),
           [
               'show_in_rest' => true,
               'rest_base'    => 'od',
               'rewrite' => [
                   'slug' => 'ofertas-demandas',
               ],
           ],
        ],
        [
           'zone',
           __('Zona', 'ramuca-cpt'),
           __('Zonas', 'ramuca-cpt'),
           [
               'show_in_rest' => true,
               'rest_base'    => 'zones',
               'rewrite' => [
                   'slug' => 'zonas',
               ],
           ],
        ],
        [
           'resource',
           __('Recurso', 'ramuca-cpt'),
           __('Recursos', 'ramuca-cpt'),
           [
               'show_in_rest' => true,
               'rest_base'    => 'resources',
               'supports' => [ 'title', 'editor', 'thumbnail' ],
               'taxonomies' => [
                    'resource_cats',
                ]
            ]
        ],
        [
           'manifest',
           __('Manifiesto', 'ramuca-cpt'),
           __('Manifiestos', 'ramuca-cpt'),
           [
               'show_in_rest' => true,
               'rewrite' => [
                   'slug' => 'firma',
               ],
           ]
        ],
    ];

   foreach($ramuca_cpt_custom_post_types as $cpt)
       call_user_func_array('ramuca_cpt_create', $cpt);
}

/**
 *  Registers a custom post type given an identifier and its singular and plural labels
 */

function ramuca_cpt_taxonomy($id, $cpt, $singular, $plural, $hierarchical=False, $overwrite=NULL)
{
    $labels = [
        'name'                       => _x( $plural, 'taxonomy general name', 'ramuca-cpt' ),
        'singular_name'              => _x( $singular, 'taxonomy singular name', 'ramuca-cpt' ),
        'search_items'               => __( 'Busca ' . $plural, 'ramuca-cpt' ),
        'popular_items'              => __( $plural . ' populares', 'ramuca-cpt' ),
        'all_items'                  => __( 'Todo ' . $plural, 'ramuca-cpt' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edita ' . $singular, 'ramuca-cpt' ),
        'update_item'                => __( 'Actualiza ' . $singular, 'ramuca-cpt' ),
        'add_new_item'               => __( 'Añade nueva ' . $singular, 'ramuca-cpt' ),
        'new_item_name'              => __( 'Nueva ' . $singular . ' Name', 'ramuca-cpt' ),
        'separate_items_with_commas' => __( 'Separa cada elemento con comas', 'ramuca-cpt' ),
        'add_or_remove_items'        => __( 'Añade o elimina elementos', 'ramuca-cpt' ),
        'choose_from_most_used'      => __( 'Elige dentro de lo más usado', 'ramuca-cpt' ),
        'not_found'                  => __( 'No se ha encontrado nada.', 'ramuca-cpt' ),
        'menu_name'                  => __( $plural, 'ramuca-cpt' ),
    ];
    $settings = [
        'hierarchical'      => $hierarchical,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'show_in_nav_menus' => true,
        'rewrite'           => [
            'slug' => sanitize_title($plural)
        ],
    ];
    if( isset($overwrite) ){
        foreach($overwrite as $k=>$v)
            $settings[$k] = $v;
    }
    register_taxonomy($id, $cpt, $settings);
}

function ramuca_cpt_categories(){
    $ramuca_categories = [
        [
            'resource_cats',
            ['resource',],
            __('Tipo de recurso', 'ramuca-cpt'),
            __('Tipos de recurso', 'ramuca-cpt'),
            True
        ],
    ];
    foreach($ramuca_categories as $cat)
        call_user_func_array('ramuca_cpt_taxonomy', $cat);
}

add_action('init', 'ramuca_cpt_categories');

/**
 *  Custom columns for Offers/Demands custom post type in admin
 */

function ramuca_cpt_od_columns($columns)
{
    return [
      'cb'    => 'Check',
      'title' => 'Title',
      'type'  => 'Tipo',
      'date'  => 'Fecha',
      'zone'  => 'Ramukita',
    ];
}

function ramuca_cpt_manage_od_columns($column_name, $post_id)
{
    if($column_name == 'type'){
        echo get_field('field_5e75562186b9d', $post_id)['label'];
    } else if($column_name == 'zone'){
        echo get_field('field_5e7b48b079900', $post_id)[0]->post_title;
    } else if($column_name == 'date'){
        echo get_the_date($post_id);
    }
    return $column_name;
}

add_filter('manage_od_posts_columns', 'ramuca_cpt_od_columns');
add_filter('manage_od_posts_custom_column', 'ramuca_cpt_manage_od_columns', 10, 2);

/**
 *  Saves an offer/demand post after form submission
 *  Assumes a lot of things, but is a quick&dirty way of setting up this feature
 */

add_action('wpcf7_mail_sent','ramuca_cpt_wpcf7_save');
add_action('wpcf7_mail_failed','ramuca_cpt_wpcf7_save');

function ramuca_cpt_wpcf7_save($contact_form)
{
    $id = $contact_form->id();
    $admin_email = get_bloginfo('admin_email');
    $posted_data = WPCF7_Submission::get_instance()->get_posted_data();

    // TODO: not hardcode this, put it into customizer mods
    if($id == 268 || $id ==300)
    {
        $new_post = [
            'post_title'   => 'fullname',
            'post_content' => 'subject',
            'post_type'    => 'od',
            'post_status'  => 'draft',
            'meta_input'   => [
                'direccion' => 'street',
                'notas' => 'contact',
            ]
        ];

        foreach($new_post as $field => $mapping)
        {
            // Map not meta fields
            if( $field != 'meta_input' ){
                if(isset($posted_data[$mapping]) && !empty($posted_data[$mapping])){
                      $new_post[$field] = $posted_data[$mapping];
                }
            // Map meta fields
            } else {
                foreach($new_post['meta_input'] as $meta => $mapping_meta){
                    if(isset($posted_data[$mapping_meta]) && !empty($posted_data[$mapping_meta])){
                          $new_post['meta_input'][$meta] = $posted_data[$mapping_meta];
                    } else {
                          // Remove unused keys
                          unset( $new_post['meta_input'][$meta] );
                    }
                }
            }
        }

        // Set up fields not depending on form fields
        $new_post['meta_input']['tipo'] = $id == 268 ? 1 : 0;
        $new_post['meta_input']['acuerdo_datos'] = True;
        $ramukita_id = $posted_data['zone'];
        $ramukita_found = isset($ramukita_id) && trim($ramukita_id) != '';
        if( $ramukita_found ){
            $ramukita_id = intval($ramukita_id);
            $new_post['meta_input']['ramukita'] = $ramukita_id;
        };
        //When everything is prepared, insert the post into your Wordpress Database
        include 'telegram-token.php';
        $post_id = null;
        if($post_id = wp_insert_post($new_post))
        {
            // If the ramukita is well known send message to its chanel
          if( $ramukita_found )
          {
              $ramukita = get_post($ramukita_id);
              $method   = get_field('field_5e7b484c6bffd', $ramukita_id);
              $contact  = get_field('field_5e7b483a7caba', $ramukita_id);
              $subject  = 'Hay una ' . ($id==268 ? 'demanda' : 'oferta') . ' de apoyo en la ramukita ' . $ramukita->post_title;

              //Everything worked, you can stop here or do whatever
              // Checks if ramukita is not configured properly to be contacted
              if(!isset($contact) || ($method == '1' && substr($contact, 0,1) != '@') || ($method == '0' && !filter_var($contact, FILTER_VALIDATE_EMAIL))){
                  wp_mail(
                      $admin_email,
                      'Una ramukita tiene problemas de configuración y no ha podido ser notificada',
                      'Ramukita: ' . $ramukita->post_title . ' | Contenido: '.get_site_url().'/wp-admin/post.php?post='.$post_id.'&action=edit'
                  );
              }

              // TELEGRAM
              if($method == '1' && isset($contact) && substr($contact, 0, 1) == '@'){
                  $data = [
                    'chat_id' => $contact,
                    'text'    => $subject,
                  ];
                  $response = file_get_contents("https://api.telegram.org/bot".$TELEGRAM_TOKEN."/sendMessage?" . http_build_query($data) );
              // EMAIL
              } else if(isset($contact) && filter_var($contact, FILTER_VALIDATE_EMAIL)){
                  wp_mail(
                      $contact,
                      $subject,
                      'Revisar: '.get_site_url().'/wp-admin/post.php?post='.$post_id.'&action=edit'
                  );
              }
          // If not contact web admin
          } else {
              wp_mail(
                  $admin_email,
                  'Hay un contenido que revisar en ramuca.net',
                  'Revisar: '.get_site_url().'/wp-admin/post.php?post='.$post_id.'&action=edit'
              );
          }
       }
    } else if($id=="53") {
        $manifest = get_post(372);
        $new_supporter = $posted_data['name'] . "," . $posted_data['id'] . "," . $posted_data['email'] . "," . $posted_data['type'] . "," . date('d/M/Y', time());
        wp_update_post([
            'id'           => 372,
            'post_content' => $new_supporter
        ]);
    }
    return;
}

/**
 * Enqueue scripts and styles.
 */

function ramuca_cpt_scripts()
{
    $od_contact_slug_offer = get_theme_mod('od_form_slug__offer');
    $od_contact_slug_demand = get_theme_mod('od_form_slug__demand');
    if( is_page([ $od_contact_slug_offer, $od_contact_slug_demand]) ){
         wp_enqueue_script( 'leaflet', plugin_dir_url(__FILE__).'js/leaflet/leaflet.js', [], '1.6', true );
  	     wp_enqueue_script( 'ramuca-cpt-get_nearest_zone', plugin_dir_url(__FILE__).'js/get-nearest-zone.js', [ 'jquery', 'leaflet' ], '20151215', true );
    };
}

add_action( 'wp_enqueue_scripts', 'ramuca_cpt_scripts' );


/**
 *   Add endpoint to get local groups
 */

function ramuca_cpt_get_zones_api_callback(WP_REST_Request $request)
{
    $zones = get_posts([
        'post_type'   => 'zone',
        'post_status' => 'publish',
        'numberposts' => -1,
    ]);

    if(empty($zones)){
        return null;
    }

    $data = array_map(function($zone)
    {
        $id = $zone->ID;
        $loc = get_field('field_5e7b4662b03ef', $id)['markers'][0];
        return [
            'name' => $zone->post_title,
            'id' => $id,
            'pos'  => [
                'lat' => $loc['lat'],
                'lon' => $loc['lng'],
            ],
        ];
    }, $zones);

    return $data;
}

add_action( 'rest_api_init', function()
{
    register_rest_route('ramuca-zones/v1', '/all', [
      'methods'  => 'GET',
      'callback' => 'ramuca_cpt_get_zones_api_callback',
    ]);
} );
