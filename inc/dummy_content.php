<?php
/**
 *  PHP Script to create dummy offers and demands using WP CLI
 *
 *  usage:  wp eval-file dummy_content.php 50
 */

// Get params from user input

// number of dummy posts to be generated
$n = $args[0];

//Loop through the CSV rows.
for($i=0; $i<$n; $i++)
{
    $id = wp_insert_post([
        'post_title'   => 'Lorem',
        'post_content' => 'Lorem ipsum',
        'post_type'    => 'od',
        'post_status'  => 'publish',
        'meta_input'   => [
            'barrio' => random_int(0, 103),
            'direccion' => 'Lorem',
            'idiomas' => 'Lorem',
            'horario_disponible' => 'Lorem',
            'telefono' => '123456789',
            'correo_electrónico' => 'example@dummy.es',
            'acuerdo_datos' => True,
            'publicar_telefono' => True,
            'notas_de_contacto' => 'Lorem',
            'tipo' => rand(0, 10)<5 ? '0' : '1',
        ]
    ]);
    WP_CLI::success('Post with ID ' . $id . ' created with success');
}
