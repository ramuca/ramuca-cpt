<?php
/**
 *  PHP Script to import posts from a given CSV using WP CLI
 *  Expects rows to be posts and column to be fields
 *  Column data:
 *  nombre completo | descripción de servicios | dirección | idiomas | horario | tlf | email | notas
 *
 *  usage:  wp eval-file import-csv.php data.csv [oferta]
 */

// Get params from user input
// input file
$file = $args[0];
// offer or demand? If not specified the type is an offer 
if( isset($args[1]) )
    $od_type = $args[1] == 'oferta' ? 0 : 1;
else
    $od_type = 0;

//Open the file.
$fileHandle = fopen($file, "r");

//Loop through the CSV rows.
while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE)
{
    $id = wp_insert_post([
        'post_title'   => $row[0],
        'post_content' => $row[1],
        'post_type'    => 'od',
        'post_status'  => 'publish',
        'meta_input'   => [
            'direccion' => $row[2],
            'idiomas' => $row[3],
            'horario_disponible' => $row[4],
            'telefono' => $row[5],
            'correo_electrónico' => $row[6],
            'acuerdo_datos' => True,
            'publicar_telefono' => True,
            'notas_de_contacto' => $row[7],
            'tipo' => $row[8],
        ],


    ]);
    WP_CLI::success('Post with ID ' . $id . ' created with success');
}
