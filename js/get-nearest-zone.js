/**
 *   Handles filters in the Offers/Demands view
 */


function capitalize(string) {
    string = string.toLowerCase();
    return string
      .split(' ')
      .map(function(word){
          return word.charAt(0).toUpperCase() + word.slice(1)
      })
      .join(' ');
}

document.addEventListener("DOMContentLoaded", function(){

    var street_input = document.querySelector('[name=street]');
    var zone         = document.querySelector('[name=zone]');

    street_input.addEventListener('blur', function(e){
        var street = capitalize(e.target.value);
        console.log("Looking for street " + street );
        var url = "https://nominatim.openstreetmap.org/search?street=" + encodeURIComponent(street) + "&town=Sevilla&city=Sevilla&country=España&format=json";
        jQuery.ajax({
            url : url,
            method: 'GET',
            success: function(result)
            {
                if(result[0]){
                    var street_latlng = L.latLng(result[0].lat, result[0].lon);

                    var zones = [];
                    jQuery.ajax({
                        url : '/wp-json/ramuca-zones/v1/all',
                        method: 'GET',
                        success: function(zones){
                            var distance = Number.MAX_VALUE;
                            var nearest = {
                                'name' : '',
                                'id'   : 0,
                            }
                            var nearest_name = '';
                            for(var i = 0, zones_number = zones.length; i < zones_number; i++){
                                var current_distance = street_latlng.distanceTo(L.latLng(
                                    zones[i].pos.lat,
                                    zones[i].pos.lon
                                ));
                                if(current_distance < distance) {
                                    distance = current_distance;
                                    nearest.name = zones[i].name;
                                    nearest.id = zones[i].id;
                                }
                            }
                            zone.value = nearest.id;
                            console.log("Nearest zone is " + nearest.name);
                        },
                    });
                    var prev_error = document.querySelector('.warning');
                    if(prev_error) prev_error.remove();
                } else {
                    var error = document.createElement("p");
                    error.classList.add('warning');
                    error.innerHTML = 'Tenemos problemas para encontrar la dirección, prueba a hacerla más sencilla, nos basta con un "Calle Alondra" o "Carretera de Carmona"';
                    street_input.insertAdjacentElement("afterend", error);
                };
            },
        })
    });
});
