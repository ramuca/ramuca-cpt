/**
 *   Handles filters in the Offers/Demands view
 */

document.addEventListener("DOMContentLoaded", function(){

    var type_filter = document.querySelector('#od-type-filter');
    var hood_filter = document.querySelector('#od-hood-filter');
    var selected_hood = hood_filter.value;
    var posts = document.querySelectorAll('.od-item');

    hood_filter.addEventListener('change', function(e){
        var value = e.target.value;
        if(value != 'all'){
            var count = {
              offers  : 0,
              demands : 0,
            }
            document.querySelectorAll('.od-list__empty').forEach( function(empty){
                empty.classList.remove('hidden');
            });
            posts.forEach( function(post){
                if(post.dataset.hood == value){
                    post.classList.remove('hidden');
                    count[post.dataset.type] += 1;
                } else {
                    post.classList.add('hidden');;
                }
                console.log(count);
                if(count['offers'] > 0){
                    document.querySelector('.od-list__empty[data-type=offers]').classList.add('hidden');
                }
                if(count['demands'] > 0){
                    document.querySelector('.od-list__empty[data-type=demands]').classList.add('hidden');
                }
            });
        } else {
            posts.forEach(function(post){ 
                post.classList.remove('hidden');
            });
        }
    });
});
